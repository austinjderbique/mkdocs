# mkdocs

Project that builds a Docker image of MKdocs which accepts a directory from your local filesystem as input and use Mkdocs to produce and serve a website. 

## Description
This project encapsulates MKdocs inside a docker container. You can create a new project, build, or serve the projects. See the shell scripts for more information.

## Installation

1. Ensure you have Docker installed locally on your machine. 
2. Clone this repository and build the image or pull from the registry.


## Usage

For the full suite, use `mkdockerize.sh` to build the docker image, build a MKDocs project, and then serve the project via HTTP. Otherwise, the various scripts are available:

- `build-image.sh`: Builds the MKDocs Docker image.
- `build-mkdocs.sh`: Build your MKDocs project.
- `mkdockerize.sh`: Builds and runs your project.
- `serve-nginx.sh`: Serves the HTML outputs using nginx.
- `serve.sh`: Serves your project using MKDocs

## Authors and acknowledgment
Austin Derbique

## License
See LICENSE for more information
