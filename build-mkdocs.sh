#!/bin/bash

rm -rf Tests/test-project/site

docker run -it \
	-v "$(pwd)/Tests/test-project:/usr/src/mkdocs/build" mkdocs:latest \
	build
