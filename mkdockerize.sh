#!/bin/bash

echo "Building Docker Image"
./build-image.sh

echo "Building MKDOCS Site"
./build-mkdocs.sh

echo "Serving Website"
./serve.sh
