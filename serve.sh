#!/bin/bash

docker run -it -p 8000:8000 \
    -v "$(pwd)/Tests/test-project:/usr/src/mkdocs/build" mkdocs:latest \
    serve
